<div align="center">
    <img src="https://gitlab.com/grzgajda/typescript-styled-flex/raw/master/logo.png" alt="typescript-styled-flex" />
</div>

<div align="center">
    <strong>Flag utility for styled-components inspired with styled-is created by yldio. I missed support for TypeScript so I wrote my own package from scratch.</strong>
    <br />
    <br />
</div>

<div align="center">
  <img src="https://img.shields.io/badge/LANG-TypeScript-%232b7489.svg?style=for-the-badge" />
  <img src="https://img.shields.io/gitlab/pipeline/grzgajda/typescript-styled-flex.svg?logo=gitlab&style=for-the-badge" />
  <img src="https://img.shields.io/badge/coverage-100%25-brightgreen.svg?logo=gitlab&style=for-the-badge" />
  <img src="https://img.shields.io/npm/v/typescript-styled-flex.svg?logo=npm&style=for-the-badge" />
  <img src="https://img.shields.io/bundlephobia/minzip/typescript-styled-flex.svg?style=for-the-badge" />
  <img src="https://img.shields.io/npm/dt/typescript-styled-flex.svg?logo=npm&style=for-the-badge" />
  <img src="https://img.shields.io/badge/built%20with-styled%20components-%23c86a89.svg?style=for-the-badge" />
</div>

<br />
<br />

`<Flex />` component built on top of _typescript-styled-is_. Solve the all problems with custom components which contains some flex styles. Thanks to extending and elasticity of _styled-components_ it's possible to use flex without any hussle!

## Installation

To use _typescript-styled-flex_ there is required another two packages - _styled-components_. Otherwise package has no sense in your project. Depends of your package manager, you can use _yarn_ or _npm_.

```shell
yarn add typescript-styled-flex
```

```shell
npm install typescript-styled-flex
```

## Usage

Package _typescript-styled-flex_ comes with two components: `<Flex>` and `<FlexItem>`. The most basic example of using Flex is simple website's skeleton:

```tsx
import React from 'react'
import Flex from 'typescript-styled-flex'

function App() {
  return (
    <Flex column={true}>
      <Flex alignCenter={true} justifyCenter={true}>Header</Flex>
      <Flex row={true}>
        <Flex column={true}>Sidebar</Flex>
        <Flex>Content</Flex>
      </Flex>
    </Flex>
  )
}
```

## API

#### `<Flex>`

**Props**

* `inline`: _(Boolean)_ prints "display: inline-flex"
* `row`: _(Boolean)_ prints "flex-direction: row"
* `rowReverse`: _(Boolean)_ prints "flex-direction: row-reverse"
* `column`: _(Boolean)_ prints "flex-direction: column"
* `columnReverse`: _(Boolean)_ prints "flex-direction: column-reverse"
* `nowrap`: _(Boolean)_ prints "flex-wrap: nowrap"
* `wrap`: _(Boolean)_ prints "flex-wrap: wrap"
* `wrapReverse`: _(Boolean)_ prints "flex-wrap: wrap-reverse"
* `justifyStart`: _(Boolean)_ prints "justify-content: flex-start"
* `justifyEnd`: _(Boolean)_ prints "justify-content: flex-end"
* `justifyCenter`: _(Boolean)_ prints "justify-content: center"
* `justifyBetween`: _(Boolean)_ prints "justify-content: space-between"
* `justifyAround`: _(Boolean)_ prints "justify-content: space-around"
* `contentStart`: _(Boolean)_ prints "align-content: flex-start"
* `contentEnd`: _(Boolean)_ prints "align-content: flex-end"
* `contentCenter`: _(Boolean)_ prints "align-content: center"
* `contentSpaceBetween`: _(Boolean)_ prints "align-content: space-between"
* `contentSpaceAround`: _(Boolean)_ prints "align-content: space-around"
* `contentStretch`: _(Boolean)_ prints "align-content: stretch"
* `alignStart`: _(Boolean)_ prints "align-items: flex-start"
* `alignEnd`: _(Boolean)_ prints "align-items: flex-end"
* `alignCenter`: _(Boolean)_ prints "align-items: center"
* `alignBaseline`: _(Boolean)_ prints "align-items: baseline"
* `alignStretch`: _(Boolean)_ prints "align-items: stretch"
* `full`: _(Boolean)_ prints "width: 100%; height: 100%; flex-basis: 100%"
* `center`: _(Boolean)_ prints "align-items: center; justify-content: center"

#### `<FlexItem>`

**Props**

* `inlineBlock` _(Boolean)_ prints "display: inline-block"
* `inlineFlex` _(Boolean)_ prints "display: inline-flex"
* `flex` _(Boolean)_ prints "display: flex"
* `order` _(Number)_ prints "order: YOUR_NUMBER"
* `basis` _(Number)_ prints "flex-basis: YOUR_NUMBER"
* `grow` _(Number)_ prints "flex-grow: YOUR_NUMBER"
* `shrink` _(Number)_ prints "flex-shrink: YOUR_NUMBER"
* `noShrink` _(Boolean)_ prints "flex-shrink: 0"