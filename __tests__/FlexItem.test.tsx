import "jest-styled-components";
import * as React from "react";
import * as Renderer from "react-test-renderer";
import FlexItem from "../src/FlexItem";

describe("component <FlexItem />", () => {
  it("should render in DOM tree", () => {
    const result = Renderer.create(<FlexItem>Test</FlexItem>).toJSON();

    expect(result).toHaveStyleRule("flex-grow", "0");
    expect(result).toMatchSnapshot();
  });

  const args = [
    ["display", "inline-block", { inlineBlock: true }],
    ["display", "inline-flex", { inlineFlex: true }],
    ["display", "flex", { flex: true }],
    ["order", "12", { order: 12 }],
    ["flex-basis", "13", { basis: 13 }],
    ["flex-grow", "4", { grow: 4 }],
    ["flex-shrink", "11", { shrink: 11 }],
    ["flex-shrink", "0", { noShrink: true }]
  ];

  it.each(args)(
    'should have "%s: %s" style rule',
    (cssKey, cssValue, props) => {
      const result = Renderer.create(
        <FlexItem {...props}>Test</FlexItem>
      ).toJSON();

      expect(result).toHaveStyleRule(cssKey, cssValue);
      expect(result).toMatchSnapshot();
    }
  );
});
